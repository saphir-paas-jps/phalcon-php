#!/bin/bash
a=`php -v |head -n1 |awk '{print $2}'|cut -f 1,2 -d .`;

sed -i '$ a\[phalcon\]\n;extension=/usr/lib64/php/modules/phalcon/phalcon-php-5.6.so\n\;extension=/usr/lib64/php/modules/phalcon/phalcon-php-7.0.so\n\;extension=/usr/lib64/php/modules/phalcon/phalcon-php-7.1.so\n\;extension=/usr/lib64/php/modules/phalcon/phalcon-php-7.2.so\n' /etc/php.ini ;

phalcon_loader_line=$(echo $(grep $a'.so' /etc/php.ini) | sed 's/^.//');
sed -i "s|;$phalcon_loader_line|$phalcon_loader_line|g" /etc/php.ini