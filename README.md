![phalcon-logo](images/phalcon-logo.png)

# Phalcon Framework Add-On

The [Phalcon Framework](https://phalconphp.com/) is a full-stack PHP framework delivered as a C-extension.

**Stacks this add-on can be applied to:** *Apache* application servers

**Supported PHP versions:**
- *5.6.*
- *7.0.*
- *7.1.*
- *7.2.*

For more information about [Jelastic Add-Ons](https://github.com/jelastic-jps/jpswiki/wiki/Jelastic-Addons) and their usage, refer to the linked guide.

## How to Install Phalcon Framework to Jelastic Environment

To install the Phalcon Framework add-on, copy link to the **_manifest.jps_** file above and [import](https://docs.jelastic.com/environment-import) it through the Jelastic dashboard at your [platform](https://jelastic.cloud/) (alternatively, it can be launched from the embedded [Jelastic Marketplace](https://docs.jelastic.com/marketplace#add-ons)).

![phalcon-installation](images/phalcon-install.png)

In the appeared installation window, specify the following details:
- **_Environment name_** - environment the Phalcon Framework should be integrated
- **_Nodes_** - target PHP application server for the add-on appliance (is fetched automatically upon selecting the environment)

Click on **Install** to proceed. Just after installation is finished, you can [deploy](https://docs.jelastic.com/whole-project-deploying) and run any PHP application in a usual way.

## How to Customize Phalcon Configurations

Since Phalcon Framework represents a PHP extension, its settings can be adjusted within **_php.ini_** configuration file at your application server. To access it, click **Config** next to the corresponding node and select this file in the leftmost _File manager_ panel.

![phalcon-config](images/phalcon-config.png)

